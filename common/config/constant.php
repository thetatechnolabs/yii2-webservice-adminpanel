<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 11/23/2015
 * Time: 5:10 AM
 */
$local = true;

//BASE URL

    if($local){
        define("BASE_URL","http://192.168.1.9");
    } else {
        define("BASE_URL","http://demo.thetatechnolabs.com");
    }

define("DEFAULT_IMAGE", "http://localhost/yii-backend/backend/web/default_image/super_admin.png");
define("DEFAULT_SALON_IMAGE", "http://192.168.1.136/yii-backend/backend/web/default_image/default_salon.png");

//APP INFO
define("APP_NAME", "Kaatar");
define("VERSION","1.0.0");
define("COMPANY_NAME","Theta Technolabs");
//Status
define("ACTIVE",1);
define("INACTIVE",0);

//Deleted
define("DELETED",0);
define("NOT_DELETED",1);

//platforms
define("WEB",0);
define("IOS",1);
define("ANDROID",2);


// Roles
define("SUPER_ADMIN",1);
define("NORMAL_USER",2);

//User's Verification Status
define("USER_VERIFIED",1);
define("USER_PENDING",0);

//some Default values
define("DEFAULT_COUNTRY_CODE",91);
define("DEFAULT_USER_ID",1);

//Login Status
define("NOT_LOGGED_IN",0);
define("LOGGED_IN",1);

//DEFUALT VALUES
define('PAGESIZE',5);

define("REST_DEFAULT_MESSAGE_STRING","oops... there is some problem in code, edison.. you can do it and we know that");
