<?php
namespace backend\controllers;
use backend\components\CustController;
use Yii;

/**
 * Salon controller
 */
class SalonsController extends CustController
{


    public function actionIndex()
    {
        return $this->render('index');
    }

}
