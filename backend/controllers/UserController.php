<?php
namespace backend\controllers;
use backend\components\CustController;
use common\models\ChangePasswordForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use kartik\growl\Growl;
use yii\helpers\Html;
/**
 * Site controller
 */
class UserController extends CustController
{


    public function actionProfile()
    {
        return $this->render('profile');
    }
    public function actionEditProfile(){
        return $this->render('editprofile');
    }


    public function actionChangePassword(){
        $model = new ChangePasswordForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
             return ActiveForm::validate($model);
        }
        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
                $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else{
                if($model->resetPassword()){
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 12000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have changed your password successfully',
                        'title' => 'Password Changed',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                  return $this->redirect(\Yii::$app->urlManager->createUrl("user/change-password"));
                }

            }
        }



        return $this->render('changepassword',['model'=>$model]);
    }

}
