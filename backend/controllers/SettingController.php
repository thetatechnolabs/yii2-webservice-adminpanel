<?php
namespace backend\controllers;
use backend\components\CustController;
use backend\models\Actions;
use backend\models\ActionSearch;
use backend\models\Controllers;
use backend\models\ControllerSearch;
use backend\models\Menus;
use backend\models\MenusSearch;
use backend\models\Rights;
use common\models\Role;
use common\models\RoleSearch;
use kartik\form\ActiveForm;
use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\Menu;

/**
 * Setting controller
 */
class SettingController extends CustController
{

    /***
     * @rohanmashiyava
     *  List of User(System User)
     *  Who user Admin Panel
     **/
    public function actionIndex() {

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=PAGESIZE;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /***
     * @rohanmashiyava
     *  Add User(System User)
     *  Who user Admin Panel
     **/
    public function actionAddUser() {
        $model = new User();
        return $this->render("edituser",[
            'model'=>$model
        ]);
    }

    /***
     * @rohanmashiyava
     *  Edit User(System User)
     **/
    public function actionEditUser($id) {
        $model = User::findIdentity($id);
        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
            $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else{
               // print_r($model->attributes);
            }
        }

        return $this->render("edituser",[
            'model'=>$model
        ]);
    }
    /***
     * @rohanmashiyava
     *  Change Status of User(System User)
     *  Who user Admin Panel
     **/
    public function actionStatusUser($id) {
        $model = User::findUserByID($id);
        if($model->status){
            $model->status =  INACTIVE;
        }else{
            $model->status =  ACTIVE;
        }
    echo $model->save(false);

    }

    /***
     * @rohanmashiyava
     *  Lists Roles(System User)
     *  All Roles for system
     **/
    public function actionAddRole() {
        $model = new Role();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
            $model->is_deleted = NOT_DELETED;
            $model->created_by = Yii::$app->user->identity->getId();
            $model->updated_by = Yii::$app->user->identity->getId();
            $model->updated_at = time();
            $model->created_at = time();
            $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else{
                if($model->save()){
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have created role successfully',
                        'title' => 'Roles Created',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/roles"));
                }

            }
        }

        return $this->render("addroles",[
            'model'=>$model
        ]);
    }
    /***
     * @rohanmashiyava
     *  Lists Roles(System User)
     *  All Roles for system
     **/
    public function actionEditRole($id)  {
        $model = Role::findOne(['id'=>$id]);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
            $model->is_deleted = NOT_DELETED;
            $model->updated_by = Yii::$app->user->identity->getId();
            $model->updated_at = time();
            $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else{
                if($model->save()){
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have updated role successfully',
                        'title' => 'Role Updated',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/roles"));
                }

            }
        }

        return $this->render("addroles",[
            'model'=>$model
        ]);
    }
    /***
     * @rohanmashiyava
     *  List Roles(System User)
     *   All Roles for system
     **/
    public function actionRoles()  {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=PAGESIZE;
        return $this->render('roles', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /***
     * @rohanmashiyava
     *  Add Roles(System User)
     *  Who user Admin Panel
     **/
    public function actionStatusRole($id) {
        $model = Role::findOne(['id'=>$id]);
        if ($model->status) {
            $model->status = INACTIVE;
        } else {
            $model->status = ACTIVE;
        }
        echo $model->save(false);
    }

    /***
     * @rohanmashiyava
     *  List Menus(System User)
     *  Who user Admin Panel
     **/
    public function actionMenus() {
        $searchModel = new MenusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('menus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
/**
 * @rohanmashiayava
 * add Menus
 **/
    public function actionAddMenu(){
        $model = new Menus();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if(Yii::$app->request->post() && $model->load(Yii::$app->request->post())){
            $cnt = Menus::find()->select('MAX(`order`)')->scalar();
            $model->order = $cnt+1;
            $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else{
                if($model->save()){
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have created Menu successfully',
                        'title' => 'Menus Created',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/menus"));
                }

            }
        }

        return $this->render("addmenus",[
            'model'=>$model
        ]);
    }
    /**
     * @rohanmashiayava
     * edit Menu
     **/
    public function actionEditMenu($id) {
        $model = Menus::findOne(['id'=>$id]);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if ($errors){
                return $errors;
            } else {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have updated Menu successfully',
                        'title' => 'Menus Updated',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/menus"));
                }

            }
        }
        return $this->render("editmenus",[
            'model'=>$model
        ]);
    }

    /**
     * @rohanmashiayava
     * dependant dropdown ajax function
     **/

    public function actionSetAction($id) {
        if (Yii::$app->request->isAjax) {
            $model = Controllers::findOne(['id'=>$id]);
            $modelcount = sizeof($model->actions);
            if($modelcount>0){
                foreach($model->actions as $action) {
                    echo "<option value='".$action->id."'>".$action->action_name."</option>";
                }
            } else {
                echo "<option>-</option>";
            }

        }
        return false;
    }
    /**
     * @rohanmashiayava
     * change Order of Menu - Up
     **/
    public function actionUp($id) {
        $model = Menus::findOne(['id'=>$id]);
        if($model){
            $recordorder  = $model->order;
            if($recordorder == 1) {
                return false;
            } else {
                $target_order = $recordorder-1;
                $current = Menus::findOne(['order'=>$recordorder]);
                $target = Menus::findOne(['order'=>$target_order]);
                $current->order = $target_order;
                $target->order = $recordorder;
                $current->save(false);
                $target->save(false);
                return true;
            }
        }
    }

    /**
     * @rohanmashiayava
     * change Order of Menu - Down
     **/
    public function actionDown($id) {
        $model = Menus::findOne(['id'=>$id]);
        if($model){
            $recordorder  = $model->order;
            $max =  Menus::find() // AQ instance
                ->select('MAX(`order`)') // we need only one column
                ->scalar();
            if($recordorder == $max){
                return false;
            } else {
                $target_order = $recordorder+1;
                $current = Menus::findOne(['order'=>$recordorder]);
                $target = Menus::findOne(['order'=>$target_order]);
                $current->order = $target_order;
                $target->order = $recordorder;
                $current->save(false);
                $target->save(false);
                return true;
            }
        }
    }

    /**
     * @rohanmashiayava
     *  Menu - Delete
     **/

    public function actionDeleteMenu($id) {
        $model = Menus::findOne(['id'=>$id]);
        $max =  Menus::find()->select('MAX(`order`)')->scalar();

        for($i=$model->order;$i<=$max;$i++){
            //$model = Menus::findOne(['id'=>$])
            if($i == $model->order) {
                $delete = Menus::findOne(['order'=>$i]);
                $delete->delete();

            } else {
                $update = Menus::findOne(['order'=>$i]);
                $update->order = $update->order-1;
                $update->save(false);
            }

        }
        return true;
    }

    /**
     * @rohanmashiayava
     *  Controller - index - View
     **/

    public function actionControllers() {
        $searchModel = new ControllerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=PAGESIZE;

        return $this->render('controllers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * @rohanmashiayava
     *  Controller - Add
     **/
    public function actionAddController() {
        $model = new Controllers();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if ($errors){
                return $errors;
            } else {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have Added Controller successfully',
                        'title' => 'Controller Added',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/controllers"));
                }

            }
        }
        return $this->render("addcontroller",[
            'model'=>$model
        ]);
    }
    /**
     * @rohanmashiayava
     *  Controller - Edit
     **/
    public function actionEditController($id) {
        $model = Controllers::findOne(['id'=>$id]);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if ($errors){
                return $errors;
            } else {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have updated Controller successfully',
                        'title' => 'Controller Updated',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/controllers"));
                }

            }
        }
        return $this->render("editcontroller",[
            'model'=>$model
        ]);
    }
    /**
     * @rohanmashiayava
     *  Controller - Delete
     **/

    public function actionDeleteController($id) {

        $model = Controllers::findOne(['id'=>$id]);
        if($model){
            $model->delete();
            return true;
        }else{
            return false;
        }

    }
    /**
     * @rohanmashiayava
     *  Action - index - View
     **/

    public function actionActions() {
        $searchModel = new ActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=PAGESIZE;

        return $this->render('actions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * @rohanmashiayava
     *  Action - Add
     **/
    public function actionAddAction() {
        $model = new Actions();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if ($errors){
                return $errors;
            } else {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have Added Action successfully',
                        'title' => 'Action Added',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/actions"));
                }

            }
        }
        return $this->render("addaction",[
            'model'=>$model
        ]);
    }
    /**
     * @rohanmashiayava
     *  Action - Edit
     **/
    public function actionEditAction($id) {
        $model = Actions::findOne(['id'=>$id]);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if ($errors){
                return $errors;
            } else {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have updated Action successfully',
                        'title' => 'Action Updated',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/actions"));
                }

            }
        }
        return $this->render("editaction",[
            'model'=>$model
        ]);
    }
    /**
     * @rohanmashiayava
     *  Actions - Delete
     **/

    public function actionDeleteAction($id) {

        $model = Actions::findOne(['id'=>$id]);
        if($model){
            $model->delete();
            return true;
        }else{
            return false;
        }

    }

    /**
     * @rohanmashiayava
     *  Right management
     **/
    public function actionRights($id = "") {
        $rights = new Rights();
        $flag = false;
        $controllers = Controllers::find()->all();
        $roleData = [];
        $moduleData = [];
        if(!empty($id)){
            $roleData = Rights::findAll(['role_id'=>$id]);
            foreach($controllers as $controller){
                foreach($controller->actions as $action){
                        $moduleData[$controller->id][] = $action->id;
                }
            }
        }
        if (Yii::$app->request->post()) {
            if(!empty($_POST['right'])){
                Rights::deleteAll(['role_id'=>$id]);
                foreach($_POST['right'] as $k => $r){
                    list($role_id,$controller_id,$action_id)= explode('_',$k);
                        $rightsmodel = new Rights();
                        $rightsmodel->role_id = $role_id;
                        $rightsmodel->controller_id = $controller_id;
                        $rightsmodel->action_id = $action_id;
                        //print_r($rightsmodel->attributes);die;
                        if($rightsmodel->save()){
                           $flag = true;
                        }
                }
                if($flag){
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 3000,
                        'icon' => 'glyphicon glyphicon-ok-sign',
                        'message' => 'You have updated rights for this role successfully',
                        'title' => 'Rights Updated',
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    return $this->redirect(\Yii::$app->urlManager->createUrl("setting/rights/$id"));
                }
            }

        }
        return $this->render("rights",[
            'controllers'=>$controllers,
            'roleData'=>$roleData,
            'moduleData'=>$moduleData,
            'id'=>$id,
        ]);
    }

}
