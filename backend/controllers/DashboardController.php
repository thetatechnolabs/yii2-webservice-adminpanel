<?php
namespace backend\controllers;
use backend\components\CustController;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class DashboardController extends CustController
{


    public function actionIndex()
    {
        return $this->render('index');
    }

}
