<?php
namespace backend\controllers;

use common\models\ForgotPasswordForm;
use common\models\User;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','forgot-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','lock'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            /*'error' => [
                'class' => 'yii\web\ErrorAction',
            ],*/
        ];
    }

    public function actionError(){

        $this->layout ="error/main";
        $exception = Yii::$app->errorHandler->exception;
        return $this->render('error',['error'=>$exception]);

    }
    public function actionLogin() {
        $this->layout = "login/main";

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("dashboard/index"));

        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("dashboard/index"));
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionForgotPassword(){

        $this->layout = "login/main";
        $model = new ForgotPasswordForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            $errors = ActiveForm::validate($model);
            if($errors){
                return $errors;
            }else {
                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'message' => 'Reset Password link has been sent to your email address',
                    'title' => 'Email Sent',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
                return $this->redirect(\Yii::$app->urlManager->createUrl("site/login"));
            }
        }
        return $this->render('forgot-password',[
            'model' => $model
        ]);
    }
    public function actionLock()
    {
        $this->layout = "login/main";

        $user = User::findIdentity(\Yii::$app->user->identity->id);
        $user->screenlock = ACTIVE;
        $user->save(false);

        $model = new LoginForm();
        $model->username = \Yii::$app->user->identity->username;
        if($model->load(Yii::$app->request->post()) && $model->unlock()){
            return $this->redirect(\Yii::$app->urlManager->createUrl("dashboard/index"));
        }
        return $this->render('lock',[
        'model' => $model,
            ]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(\Yii::$app->urlManager->createUrl("site/login"));
    }
}
