<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 10/6/16
 * Time: 7:14 PM
 */
namespace backend\components;
use Yii;

class Helper {

     public static function isMenuActive($controller,$action){
         if(Yii::$app->controller->id == $controller && Yii::$app->controller->action->id == $action){
             return "active";
         }else{
             return "";
         }
     }
}