<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 10/6/16
 * Time: 7:14 PM
 */
namespace backend\components;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class CustController extends Controller{


    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if(Yii::$app->user->isGuest){
            $this->redirect(\Yii::$app->urlManager->createUrl("site/login"));
            return false; //not run the action
        }else{
            if(Yii::$app->commonfunction->isLocked()){
                $this->redirect(\Yii::$app->urlManager->createUrl("site/lock"));
            }else{
                return true;
            }
        }

        return true; // continue to run action
    }

}