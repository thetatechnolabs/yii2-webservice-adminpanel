<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 16/2/16
 * Time: 4:30 PM
 */


namespace backend\components;


use backend\models\Menus;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;

class CommonFunctions extends Component
{
    public function welcome() {
        echo "Hello..Welcome to MyComponent";
    }

    public function sendReservationEmail($content) {

        $result = $this->sendEmail('reservation',$content,"One New Request for Reservation");
        print_r($result);die;
    }
    public function sendTrialEmail() {
        $content = ["content"=>"rohan"];
        return $this->sendEmail('html',$content,"One New Request for Reservation");
       // print_r($result);die;
    }

    public function sendEmail($view,$content_array,$subject) {

        return \Yii::$app->mailer->compose($view, $content_array)
               ->setFrom(["noreply@thetatechnolabs.com" => 'Reservation Request'])
               ->setTo(ADMIN_EMAIL)
               ->setSubject($subject)
               ->send();


    }
    public function getGenderName($gender){

        if($gender == 0){
            return "Not Specified";
        }elseif($gender == 1){
            return "Male";
        }elseif($gender){
            return "Female";
        }
    }

    public function isLocked(){
        if(\Yii::$app->user->identity->screenlock){
            return true;
        }else{
            return false;
        }
    }

    public function ifChecked($roledata,$setting){
        if(($roledata)){
            $flag =false;
            foreach($roledata as $r){
                if($r->role_id == $setting['role'] && $r->controller_id == $setting['controller'] && $r->action_id == $setting['action']){
                        $flag = true;
                }
            }
            return $flag;

        }else{
            return false;
        }
    }
    public function getMenu(){
        $menus = Menus::find()->all();
        $str ="";
        foreach($menus as $menu){
           return  Html::tag('li', Html::tag('a', Html::encode('k'), ['class' => '']), ['class' => '']);
           /* $str ='
            <li><a href="<?=\yii\helpers\Url::toRoute("dashboard/index")?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>';*/
        }


    }


}