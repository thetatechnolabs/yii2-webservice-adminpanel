<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 17/6/16
 * Time: 1:04 AM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<?php echo \Yii::$app->view->renderFile('@backend/views/layouts/message_panel.php');?>
<div class="right_col" role="main" style="height: 100%;
    min-height: 928px;">
    <div class="">
        <div class="x_panel">
            <div class="x_title">
                <h2>Roles & Rights</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="pull-right">
                    <a class="btn btn-app" href="<?= \yii\helpers\Url::toRoute ('setting/index') ?>">
                        <i class="fa fa-hand-o-left"></i> Back to Settings
                    </a>
                </div>
                <div class="pull-left">
                    <div class="form-group" style="width: 230px !important;">
                        <?=
                        \kartik\select2\Select2::widget ([
                            'name'    => 'roles',
                            'value'   => isset($id)?$id:"",
                            'data'    => \yii\helpers\ArrayHelper::map (\common\models\Role::find ()->all(), 'id', 'role_name'),
                            'options' => ['placeholder' => 'Select  Roles', 'id' => 'roles'],
                        ]);
                        ?>

                    </div>
                </div>
                <div>
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'id' => 'rights-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation'=>true,
                    ])?>
            <?php foreach($controllers  as $controller):?>
                <div class="x_content">
                    <p><span style="font-size: large;font-weight: bold"><?=$controller->slug?></span>
                        <input type="checkbox" id="<?=$controller->controller_name?>" class="flat"> ( Select All ) </p>

                    <div class="">
                        <ul class="to_do">
                        <?php foreach($controller->actions  as $action):?>
                            <li>
                                <p>
                                    <input type="checkbox" <?= (Yii::$app->commonfunction->ifChecked($roleData,[
                                        'role'=> isset($id)?$id:"",'controller'=>$controller->id,'action'=>$action->id]))?"checked":""; ?> name="right[<?=isset($id)?$id:""."_".$controller->id."_".$action->id?>]" class="flat <?=$controller->controller_name?>"> <?=$action->slug?> </p>
                            </li>

                        <?php endforeach;?>

                        </ul>
                    </div>
                </div>
                <?php
                $this->registerJs ("
                                $(document).ready(function(){
                                 $(function () {
                                    var checkAll = $('input#".$controller->controller_name."');
                                    var checkboxes = $('input.".$controller->controller_name."');

                                        checkAll.on('ifChecked ifUnchecked', function(event) {
                                            if (event.type == 'ifChecked') {
                                                checkboxes.iCheck('check');
                                            } else {
                                                checkboxes.iCheck('uncheck');
                                            }
                                        });
                                        checkboxes.on('ifChanged', function(event){
                                            if(checkboxes.filter(':checked').length == checkboxes.length) {
                                                checkAll.prop('checked', 'checked');
                                            } else {
                                                checkAll.removeProp('checked');
                                            }
                                            checkAll.iCheck('update');
                                        });
                                    });


                                });
                                ");
                ?>
                    <?php endforeach;?>

            </div>
                <?= \yii\bootstrap\Html::submitButton('Submit', ['class'=> 'btn btn-success pull-right']) ;?>
                <?php \yii\widgets\ActiveForm::end() ?>

            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs ("$(document).ready(function(){
$('#roles').change(function(){
    if(this.value != undefined && this.value !=''){
    var v = this.value;
       window.location.href='". \Yii::$app->urlManager->createUrl('setting/rights')."/'+v;
    }
});

 });");
?>




