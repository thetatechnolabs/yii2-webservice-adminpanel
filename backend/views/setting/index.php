<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 17/6/16
 * Time: 1:04 AM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


?>
<div class="right_col" role="main" style="height: 100%;
    min-height: 928px;">
    <div class="">
        <div class="x_panel">
            <div class="x_title">
                <h2>Settings</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="pull-right">
                    <a class="btn btn-app" href="<?= \yii\helpers\Url::toRoute('setting/menus') ?>">
                        <i class="fa fa-navicon"></i> Menus
                    </a>
                    <a class="btn btn-app" href="<?= \yii\helpers\Url::toRoute('setting/roles') ?>">
                        <i class="fa fa-user"></i> Roles
                    </a>
                    <a class="btn btn-app" href="<?= \yii\helpers\Url::toRoute('setting/rights') ?>">
                        <i class="glyphicon glyphicon-fire"></i> Rights
                    </a>
                </div>
                <div class="pull-left">
                    <a class="btn btn-app" href="<?= \yii\helpers\Url::toRoute('setting/add-user') ?>">
                        <i class="fa fa-user-plus"></i> Add User
                    </a>
                </div>
                <?php Pjax::begin(['id' => 'users']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => "",
                    'showOnEmpty'=>true,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'first_name',
                        'last_name',
                        'roles.role_name',
                        'created_at:datetime',
                        [
                            'attribute' => 'status',
                            'format'=>'raw',
                            'value' => function ($model) {
                                //return ($model->status)?"<span class='label label-success pull-right'>Active</span>":"InActive";
                                return ($model->status)? Html::tag('span', 'Active', ['class' => ['label','label-success']]):Html::tag('span', 'InActive', ['class' => ['label','label-danger']]);
                            },
                        ],


                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}  {view}  {status}  ',
                            'buttons' => [
                                'status' => function ($url, $model) {
                                    if($model->role != SUPER_ADMIN){
                                        if($model->status == ACTIVE){
                                            return Html::a('<span class="glyphicon glyphicon-ok" style=""></span>', $url,
                                                [
                                                    'class'          => 'ajaxStatus',
                                                    'title' => Yii::t('app', 'Status'),
                                                    'status-url'     => $url,
                                                    'pjax-container' => 'users',
                                                    'data-pjax'=>'w0',
                                                ]);
                                        }else{
                                            return Html::a('<span class="glyphicon glyphicon-ok" style="color:green"></span>', $url,
                                                [
                                                    'class'          => 'ajaxStatus',
                                                    'title' => Yii::t('app', 'Status'),
                                                    'data-pjax'=>'w0',
                                                    'status-url'     => $url,
                                                    'pjax-container' => 'users',
                                                ]);
                                        }
                                    }
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {

                                if ($action === 'update') {
                                    return \yii\helpers\Url::to(['setting/edit-user/'.$key]);
                                }
                                if ($action === 'status') {
                                    return \yii\helpers\Url::to(['setting/status-user/'.$key]);
                                }
                            }
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>

            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("
$(document).on('ready pjax:success', function () {
    $('.ajaxStatus').on('click', function (e) {
        e.preventDefault();
        var Url     = $(this).attr('status-url');
        var pjaxContainer = $(this).attr('pjax-container');
            $.ajax({
                    url:   Url,
                    type:  'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.'
                            + xhr.responseText);
                        }
                }).done(function (data) {
                  $.pjax.reload({container: '#' + $.trim(pjaxContainer)});
                });

    });
});

");
?>