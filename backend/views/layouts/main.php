<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 20/5/16
 * Time: 11:38 AM
 */

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php $this->title = ucwords(Yii::$app->controller->id) . ":" . APP_NAME; ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<!-- start: first top navigation bar -->
<?php echo $this->render('first_nav'); ?>
<!-- end: first top navigation bar -->

<!-- start: second top navigation bar -->
<?php echo $this->render('second_nav'); ?>
<!-- end: second top navigation bar -->

<!-- start: show content (and check, if exists a sublayout -->

<div class="container">
    <?php echo $content; ?>
</div>
<!-- end: show content -->
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

