<div id="topbar-second" class="topbar">
    <div class="container">
        <ul class="nav ">
            <li class="visible-md visible-lg <?=\backend\components\Helper::isMenuActive('dashboard','index');?> dashboard">
                <a class="<?=\backend\components\Helper::isMenuActive('dashboard','index');?> dashboard" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('dashboard') ?>"><i
                        class="fa fa-tachometer"></i><br/>Dashboard</a></li>
            <li class="visible-md visible-lg <?=\backend\components\Helper::isMenuActive('salons','index');?> directory">
                <a class="<?=\backend\components\Helper::isMenuActive('salons','index');?> directory" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('salons') ?>"><i
                        class="fa fa-scissors"></i><br/>Salons & Spa</a>
            </li>
            <li class="visible-md visible-lg  directory">
                <a class=" directory" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('users') ?>"><i
                        class="fa fa-users"></i><br/>Users</a>
            </li>

            <li class="dropdown visible-xs visible-sm">
                <a href="#" id="top-dropdown-menu" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-align-justify"></i><br>
                    Menu <b class="caret"></b></a>
                <ul class="dropdown-menu">

                    <li class="active">
                        <a class=" active dashboard" href="javascript:void(0);">Dashboard</a></li>
                    <li class="">
                        <a class=" directory" href="javascript:void(0);">Directory</a></li>

                </ul>
            </li>
        </ul>

    </div>
</div>