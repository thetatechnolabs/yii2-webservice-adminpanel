<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 10/6/16
 * Time: 5:15 PM
 */
?>

<div class="row">
    <div class="col-md-12 layout-content-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Salon & Spa</strong> administration
            </div>
           <!-- <div class="tab-menu">

                <ul class="nav nav-tabs">
                    <li class=" active">
                        <a href="<?php /*echo Yii::$app->homeUrl*/?>/index.php?r=admin%2Fuser%2Findex">Users</a></li>
                    <li class="">
                        <a href="<?php /*echo Yii::$app->homeUrl*/?>/index.php?r=admin%2Fauthentication">Settings</a></li>
                    <li class="">
                        <a href="<?php /*echo Yii::$app->homeUrl*/?>/index.php?r=admin%2Fuser-profile">Profiles</a></li>
                    <li class="">
                        <a href="<?php /*echo Yii::$app->homeUrl*/?>/index.php?r=admin%2Fgroup">Groups</a></li>
                </ul>
            </div>
-->
            <div class="panel-body">
                <div class="table-responsive">
                    <div class="pull-right">
                        <a class="btn btn-success" href="/humhub/index.php?r=admin%2Fuser%2Fadd" data-ui-loader=""><i
                                class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add new user</a> <a
                            class="btn btn-success" href="/humhub/index.php?r=user%2Finvite" data-target="#globalModal"><i
                                class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp;Send invite</a></div>

                    <div id="h517274w0" class="grid-view">
                        <div class="summary">Showing <b>1-3</b> of <b>3</b> items.</div>
                        <table class="table table-hover">
                            <colgroup>
                                <col style="width:40px;">
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col style="width:80px; min-width:80px;">
                            </colgroup>
                            <thead>
                            <tr>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=id" data-sort="id">ID</a>
                                </th>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=username"
                                       data-sort="username">Username</a></th>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=email" data-sort="email">Email</a>
                                </th>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=profile.firstname"
                                       data-sort="profile.firstname">Firstname</a></th>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=profile.lastname"
                                       data-sort="profile.lastname">Lastname</a></th>
                                <th><a href="/humhub/index.php?r=admin%2Fuser%2Findex&amp;sort=last_login"
                                       data-sort="last_login">Last login</a></th>
                                <th class="action-column">Actions</th>
                            </tr>
                            <tr id="h517274w0-filters" class="filters">
                                <td><input type="text" class="form-control" name="UserSearch[id]"></td>
                                <td><input type="text" class="form-control" name="UserSearch[username]"></td>
                                <td><input type="text" class="form-control" name="UserSearch[email]"></td>
                                <td><input type="text" class="form-control" name="UserSearch[profile.firstname]"></td>
                                <td><input type="text" class="form-control" name="UserSearch[profile.lastname]"></td>
                                <td><input type="text" id="usersearch-last_login" class="form-control"
                                           name="UserSearch[last_login]" style="width:86px;">
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-key="1">
                                <td>1</td>
                                <td>rohan_zero</td>
                                <td>rohanmashiyava@gmail.com</td>
                                <td>rohan</td>
                                <td>mashiyava</td>
                                <td>Feb 5, 2017</td>
                                <td><a class="btn btn-primary btn-xs tt"
                                       href="/humhub/index.php?r=user%2Fprofile&amp;uguid=418cdbcf-a289-4766-af63-be1e6487dd31"><i
                                            class="fa fa-eye"></i></a> <a class="btn btn-primary btn-xs tt"
                                                                          href="/humhub/index.php?r=admin%2Fuser%2Fedit&amp;id=1"><i
                                            class="fa fa-pencil"></i></a> <a class="btn btn-danger btn-xs tt"
                                                                             href="/humhub/index.php?r=admin%2Fuser%2Fdelete&amp;id=1"><i
                                            class="fa fa-times"></i></a></td>
                            </tr>
                            <tr data-key="2">
                                <td>2</td>
                                <td>david1986</td>
                                <td>david.roberts@example.com</td>
                                <td>David</td>
                                <td>Roberts</td>
                                <td>never</td>
                                <td><a class="btn btn-primary btn-xs tt"
                                       href="/humhub/index.php?r=user%2Fprofile&amp;uguid=8e147f92-65ec-482c-9488-a1ad2ea3d14d"><i
                                            class="fa fa-eye"></i></a> <a class="btn btn-primary btn-xs tt"
                                                                          href="/humhub/index.php?r=admin%2Fuser%2Fedit&amp;id=2"><i
                                            class="fa fa-pencil"></i></a> <a class="btn btn-danger btn-xs tt"
                                                                             href="/humhub/index.php?r=admin%2Fuser%2Fdelete&amp;id=2"><i
                                            class="fa fa-times"></i></a></td>
                            </tr>
                            <tr data-key="3">
                                <td>3</td>
                                <td>sara1989</td>
                                <td>sara.schuster@example.com</td>
                                <td>Sara</td>
                                <td>Schuster</td>
                                <td>never</td>
                                <td><a class="btn btn-primary btn-xs tt"
                                       href="/humhub/index.php?r=user%2Fprofile&amp;uguid=a89f2554-692c-4243-b544-a9081ee9c0dd"><i
                                            class="fa fa-eye"></i></a> <a class="btn btn-primary btn-xs tt"
                                                                          href="/humhub/index.php?r=admin%2Fuser%2Fedit&amp;id=3"><i
                                            class="fa fa-pencil"></i></a> <a class="btn btn-danger btn-xs tt"
                                                                             href="/humhub/index.php?r=admin%2Fuser%2Fdelete&amp;id=3"><i
                                            class="fa fa-times"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

