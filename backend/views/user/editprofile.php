<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 17/6/16
 * Time: 3:52 PM
 */
?>

<div class="right_col" role="main">

    <div class="">
        <div class="x_panel" style="height:768px;">
            <div class="x_title">
                <h2>Edit Profile</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <!-- start form for validation -->
                <form id="demo-form" data-parsley-validate="" novalidate="">
                    <label for="fullname">Full Name * :</label>
                    <input type="text" id="fullname" class="form-control" name="fullname" required="">

                    <label for="email">Email * :</label>
                    <input type="email" id="email" class="form-control" name="email" data-parsley-trigger="change"
                           required="">


                </form>
                <!-- end form for validations -->

            </div>
        </div>
    </div>
</div>
