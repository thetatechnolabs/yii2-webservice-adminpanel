<?php
/**
 * Created by PhpStorm.
 * User: rohan
 * Date: 17/6/16
 * Time: 12:17 AM
 */ ?>
<div class="container profile-layout-container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default panel-profile">

                <div class="panel-profile-header">

                    <div class="image-upload-container" style="width: 100%; height: 100%; overflow:hidden;">
                        <!-- profile image output-->
                        <img class="img-profile-header-background" id="user-banner-image"
                             src="/humhub/img/default_banner.jpg" width="100%" style="width: 100%; max-height: 192px;">

                        <!-- check if the current user is the profile owner and can change the images -->
                        <form class="fileupload" id="bannerfileupload" action="" method="POST"
                              enctype="multipart/form-data"
                              style="position: absolute; top: 0; left: 0; opacity: 0; width: 100%; height: 100%;">
                            <input type="file" name="bannerfiles[]">
                        </form>


                        <div class="image-upload-loader" id="banner-image-upload-loader" style="padding: 50px 350px;">
                            <div class="progress image-upload-progess-bar" id="banner-image-upload-bar">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="00"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                </div>
                            </div>
                        </div>


                        <!-- show user name and title -->
                        <div class="img-profile-data">
                            <h1><?= Yii::$app->user->identity->first_name;?></h1>

                            <h2>System Administration</h2>
                        </div>

                        <!-- check if the current user is the profile owner and can change the images -->
                        <div class="image-upload-buttons" id="banner-image-upload-buttons" style="display: none;">
                            <a href="#" onclick="javascript:$('#bannerfileupload input').click();"
                               class="btn btn-info btn-sm"><i class="fa fa-cloud-upload"></i></a>
                            <a id="banner-image-upload-edit-button" style="display: none;"
                               href="/humhub/index.php?r=user%2Faccount%2Fcrop-banner-image&amp;userGuid=418cdbcf-a289-4766-af63-be1e6487dd31"
                               class="btn btn-info btn-sm" data-target="#globalModal" data-backdrop="static"><i
                                    class="fa fa-edit"></i></a>
                        </div>
                    </div>

                    <div class="image-upload-container profile-user-photo-container"
                         style="width: 140px; height: 140px;">

                        <a data-toggle="lightbox" data-gallery="" href="javascript:void(0);">
                            <img class="img-rounded profile-user-photo" id="user-profile-image"
                                 src="/humhub/uploads/profile_image/418cdbcf-a289-4766-af63-be1e6487dd31.jpg?m=1486291123"
                                 data-src="holder.js/140x140" alt="140x140" style="width: 140px; height: 140px;">
                        </a>

                    </div>


                </div>

                <div class="panel-body">

                    <div class="panel-profile-controls">
                        <!-- start: User statistics -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="statistics pull-left">

                                    <a href="javascript:void(0);"
                                       data-target="#globalModal">
                                        <div class="pull-left entry">
                                            <span class="count">0</span>
                                            <br>
                                            <span class="title">Friends</span>
                                        </div>
                                    </a>

                                    <a href="javascript:void(0);"
                                       data-target="#globalModal">
                                        <div class="pull-left entry">
                                            <span class="count">0</span>
                                            <br>
                                            <span class="title">Followers</span>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);"
                                       data-target="#globalModal">
                                        <div class="pull-left entry">
                                            <span class="count">0</span>
                                            <br>
                                            <span class="title">Following</span>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);"
                                       data-target="#globalModal">
                                        <div class="pull-left entry">
                                            <span class="count">1</span><br>
                                            <span class="title">Spaces</span>
                                        </div>
                                    </a>
                                </div>
                                <!-- end: User statistics -->

                                <div class="controls controls-header pull-right">
                                    <a class="btn btn-primary edit-account"
                                       href="/humhub/index.php?r=user%2Faccount%2Fedit">Edit account</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 layout-nav-container">

            <!-- start: list-group navi for large devices -->
            <div class="panel panel-default">


                <div class="panel-heading"><strong>Profile</strong> menu</div>
                <div class="list-group">
                    <a class=" active list-group-item"
                       href="javascript:void(0);"><span>About</span></a>
                </div>


            </div>
            <!-- end: list-group navi for large devices -->
        </div>

        <div class="col-md-10 layout-content-container">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>About</strong> this user</div>
                <div class="panel-body">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active">
                            <a href="#profile-category-1" data-toggle="tab">General</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile-category-1">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        First name </label>
                                    <div class="col-sm-9">
                                        <p class="form-control-static">rohan</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Last name </label>
                                    <div class="col-sm-9">
                                        <p class="form-control-static">mashiyava</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Title </label>
                                    <div class="col-sm-9">
                                        <p class="form-control-static">System Administration</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User Profile</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Report
                            <small>Activity report</small>
                        </h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                            <div class="profile_img">

                                <!-- end of image cropping -->
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <img class="img-responsive avatar-view" src="<?= DEFAULT_IMAGE ?>" alt="Avatar"
                                         title="Change the avatar">
                                </div>
                                <!-- end of image cropping -->

                            </div>
                            <h3><?= \Yii::$app->user->identity->first_name . " " . \Yii::$app->user->identity->last_name; ?></h3>

                            <ul class="list-unstyled user_data">
                                <li>
                                    <i class="fa fa-envelope-o user-profile-icon"></i> <?= \Yii::$app->user->identity->email; ?>
                                </li>
                                <!--<li><i class="fa fa-map-marker user-profile-icon"></i> San Francisco, California, USA
                                </li>

                                <li>
                                    <i class="fa fa-briefcase user-profile-icon"></i> Software Engineer
                                </li>
                                <li class="m-top-xs">
                                    <i class="fa fa-external-link user-profile-icon"></i>
                                    <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                                </li>-->
                            </ul>

                            <a href="<?= \yii\helpers\Url::toRoute('user/edit-profile') ?>" class="btn btn-success"><i
                                    class="fa fa-edit m-right-xs"></i> Edit Profile</a>
                            <br/>
                            <a href="<?= \yii\helpers\Url::toRoute('user/change-password') ?>"
                               class="btn btn-warning"><i class="fa fa-user m-right-xs"></i> Change Password</a>
                            <br/>

                            <!-- start skills -->
                            <h4>Skills</h4>
                            <ul class="list-unstyled user_data">
                                <li>
                                    <p>Web Applications</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar"
                                             data-transitiongoal="50"></div>
                                    </div>
                                </li>
                                <li>
                                    <p>Website Design</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar"
                                             data-transitiongoal="70"></div>
                                    </div>
                                </li>
                                <li>
                                    <p>Automation & Testing</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar"
                                             data-transitiongoal="30"></div>
                                    </div>
                                </li>
                                <li>
                                    <p>UI / UX</p>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar"
                                             data-transitiongoal="50"></div>
                                    </div>
                                </li>
                            </ul>
                            <!-- end of skills -->

                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">

                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab"
                                                                              role="tab" data-toggle="tab"
                                                                              aria-expanded="true">User Details</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                         aria-labelledby="home-tab">

                                        <!-- start recent activity -->
                                        <div class="row">
                                            <div class="col-lg-6"><strong>First Name</strong></div>
                                            <div class="col-lg-6"><?= \Yii::$app->user->identity->first_name; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6"><strong>Last Name</strong></div>
                                            <div class="col-lg-6"><?= \Yii::$app->user->identity->last_name; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6"><strong>Username</strong></div>
                                            <div class="col-lg-6"><?= \Yii::$app->user->identity->username; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6"><strong>Email</strong></div>
                                            <div class="col-lg-6"><?= \Yii::$app->user->identity->email; ?></div>
                                        </div>
                                        <!-- end recent activity -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
