<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Search controller
 */
class SearchController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }
}