<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme-assets/css/base.css',
        'theme-assets/css/morphext.css',
        'theme-assets/css/skins/square/yellow.css',
        'theme-assets/css/font-awesome/css/font-awesome.min.css',
        'theme-assets/css/ion.rangeSlider.css',
        'theme-assets/css/ion.rangeSlider.skinFlat.css',
    ];
    public $js = [
        'theme-assets/js/common_scripts_min.js',
        'theme-assets/js/functions.js',
        'theme-assets/js/morphext.min.js',
        'theme-assets/js/holder.js',
        'theme-assets/js/icheck.js',
        'theme-assets/js/map.js',
        'theme-assets/js/ion.rangeSlider.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
