<!-- SubHeader =============================================== -->
<section class="parallax-window" id="home" data-parallax="scroll"
         data-image-src="<?php echo Yii::$app->homeUrl ?>theme-assets/images/web_training2_thumb.jpg"
         data-natural-width="1400" data-natural-height="550">
    <div id="subheader">
        <div id="sub_content">
            <a href="<?php echo Yii::$app->homeUrl;?>"><img src="<?php echo Yii::$app->homeUrl;?>/theme-assets/images/logo.png" height="120px"
                                                            style="margin-top: -15px;"></a>
            <h1>Find Best <strong id="js-rotating">Salon,Spa</strong> In Ahmedabad</h1>

            <p>Get best offer on all services.</p>

            <form method="get" action="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['search']) ?>">
                <div id="custom-search-input">
                    <div class="input-group ">
                        <input type="text" class=" search-query" name="q" placeholder="Search by Name or Service">
                        <span class="input-group-btn">
                        <input type="submit" class="btn_search" value="submit">
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <!-- End sub_content -->
    </div>
    <!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="white_bg">
    <div class="container margin_60">

        <div class="main_title">
            <p> Search salon by services </p>
        </div>
        <div class="row text-center">
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/woman-hair-cut.svg" height="65px"
                         width="65px">

                    <p class="h4">Hair cut for Her</p>
                </a>
            </div>
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/hair-cut.svg" height="65px"
                         width="65px">

                    <p class="h4">Hair cut for Him</p>
                </a>
            </div>
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/manicure.svg" height="65px"
                         width="65px">

                    <p class="h4">Manicure</p>
                </a>
            </div>
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/tints.svg" height="65px" width="65px">

                    <p class="h4">Hair Tints</p>
                </a>
            </div>
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/brush.svg" height="65px" width="65px">

                    <p class="h4">Grooming for Him</p>
                </a>
            </div>
            <div class="col-md-2">
                <a href="javascript:void(0);" class="text-center">
                    <img class="img-responsive center-block"
                         src="<?php echo Yii::$app->homeUrl ?>/theme-assets/img/hairdryer.svg" height="65px"
                         width="65px">

                    <p class="h4">Hair Treatments</p>
                </a>
            </div>

        </div>
        <!-- End row -->

    </div>
    <!-- End container -->
</div><!-- End white_bg -->

<div class="white_bg">
    <div class="container margin_60">

        <div class="main_title">
            <h2 class="nomargin_top">Choose from Top Rated</h2>

            <p>
                Most Popular Salons in town
            </p>
        </div>
        <div class="row">

            <?php for ($i = 0; $i < 6; $i++): ?>
        <div class="col-md-4">
            <div class="salon-card">
                <a href="javascript:void(0)"
                   class="relative top-res-box-bg pl10 ptop0"
                   style="background-size: cover; background-image: url('<?php echo DEFAULT_SALON_IMAGE; ?>'); height: 150px; display: block; border-radius: 4px; background-position: center center;">
                    <div class="pull-right">
                        <span class="btn btn-sm btn-offer"> * 5 offers</span>
                    </div>
                    <div class="pull-left">
                        <div class="un-fav"><i class="fa fa-heart fa-2x"></i></div>
                    </div>
                    <div class="new-salon-logo">
                        <img class="img-circle img-responsive img-logo" data-src="holder.js/100x100"
                             src="https://s-media-cache-ak0.pinimg.com/736x/3d/83/2c/3d832cf28259e98de87247f208976851.jpg"
                             height="100px" width="100px" alt="">
                    </div>
                    <div class="bottom-block">
                        <div class="col-md-6">
                            <i class="fa fa-venus-mars"></i> Unisex
                        </div>
                        <div class="col-md-6"> 45 % <i class="fa fa-thumbs-o-up"></i></div>
                    </div>
                </a>

                <div class="salon_details">
                    <a href="javascript:void(0)">
                        <div class="h4" title="Beehive">
                            Huss Hair & Beauty
                        </div>
                    </a>

                    <div class="">Maninagar, Ahmedabad</div>
                </div>
            </div>
        </div>
            <?php endfor; ?>
    </div>
    <!-- End row -->

</div><!-- End container -->
</div><!-- End white_bg -->

<div class="high_light">
    <div class="container">
        <h3>Near by Salons</h3>

        <p>Don't wanna go far? Get salons of your area here</p>
        <a href="javascript:void(0);">Get Near by</a>
    </div>
    <!-- End container -->
</div><!-- End hight_light -->

<section class="parallax-window" data-parallax="scroll"
         data-image-src="http://www.ansonika.com/quickfood/img/bg_office.jpg" data-natural-width="1200"
         data-natural-height="600">
    <div class="parallax-content">
        <div class="sub_content">
            <h3>We are coming soon on <i class="fa fa-apple"></i> & <i class="fa fa-android"></i></h3>

            <p>We'll send you a link, open it on your phone to download the app</p>

            <div class="bd-example" data-example-id="">
                <form class="form-inline" _lpchecked="1">
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="+91 "
                           autocomplete="off">
                    <button type="submit" class="btn btn-search">SMS me App Link</button>
                </form>
                <div class="text-center or">OR</div>
                <form class="form-inline" _lpchecked="1">
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                           placeholder="Enter your email Id" autocomplete="off">
                    <button type="submit" class="btn btn-search">Email me App Link</button>
                </form>
            </div>
        </div>
        <!-- End sub_content -->
    </div>
    <!-- End subheader -->
</section><!-- End section -->
<!-- End Content =============================================== -->


