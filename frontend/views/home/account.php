<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll"
         data-image-src="<?php echo Yii::$app->homeUrl ?>theme-assets/img/login-singup.jpg"
         data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
        <div id="sub_content">
            <a href="<?php echo Yii::$app->homeUrl;?>"><img src="<?php echo Yii::$app->homeUrl;?>/theme-assets/images/logo.png" height="90px"
                                   style="margin-top: -15px;"></a>

            <h1> Sign up or log in </h1>

        </div>
        <!-- End sub_content -->
    </div>
    <!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->
<div class="container margin_60">
    <div class="main_title margin_mobile">
        <h2 class="nomargin_top">Hurry up, Login Now !</h2>

        <p>
            <span class="lead"><strong>230</strong></span> offers is waiting for you.
        </p>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name_contact" name="name_contact"
                                   placeholder="Username">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="lastname_contact" name="lastname_contact"
                                   placeholder="Password">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                            <button class="btn_full_outline">Login</button>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <a href="javascript:void(0);"> <img class="login-social" src="<?php echo Yii::$app->homeUrl?>theme-assets/images/facebook_login.png"> </a>
                </div>
                <div class="col-md-6">
                    <a href="javascript:void(0);"> <img class="login-social" src="<?php echo Yii::$app->homeUrl?>theme-assets/images/facebook_login.png"> </a>
                </div>
            </div>
        </div>
        <!-- End col  -->
    </div>
    <!-- End row  -->
</div>
<!-- Content ================================================== -->
<div class="container margin_60">
    <div class="main_title margin_mobile">
        <h2 class="nomargin_top">Sign up for free</h2>

        <p> <span><strong>25</strong> salons awaited for your visit.</span></p>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name_contact" name="name_contact"
                                   placeholder="First Name (e.g. Ashish)">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="lastname_contact" name="lastname_contact"
                                   placeholder="Last Name (e.g. Nair)">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="email" id="email_contact" name="email_contact" class="form-control "
                                   placeholder="Email (e.g ashish@email.com)">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <input type="text" id="phone_contact" name="phone_contact" class="form-control"
                                   placeholder="+91 98 250 98 250">
                        </div>
                    </div>
                </div>
                <!-- End row  -->
                <div id="pass-info" class="clearfix"></div>
                <div class="row">
                    <div class="col-md-6">
                        <label><input name="mobile" type="checkbox">Accept <a href="#0">terms
                                and conditions</a>.</label>
                    </div>
                </div>
                <!-- End row  -->
                <hr style="border-color:#ddd;">
                <div class="text-center">
                    <button class="btn_full_outline">Submit</button>
                </div>
            </form>
        </div>
        <!-- End col  -->
    </div>
    <!-- End row  -->
</div>
<?php echo $this->registerJs("
$(function () {
		 'use strict';
		 $('input').iCheck();
        $('#range').ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 15,
            from: 0,
            to:5,
            type: 'double',
            step: 1,
            prefix: 'Km',
            grid: true
        });
    });
"); ?>



