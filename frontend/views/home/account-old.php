<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll"
         data-image-src="<?php echo Yii::$app->homeUrl ?>theme-assets/img/login-singup.jpg"
         data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
        <div id="sub_content"><img src="/yii-backend/theme-assets/images/logo.png" height="90px"
                                   style="margin-top: -15px;">

            <h1> Sign up or log in </h1>

        </div>
        <!-- End sub_content -->
    </div>
    <!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Content ================================================== -->
<div class="container margin_60_35">
    <div class="row">

        <div class="col-md-2"></div>
        <div class="col-md-8 ">
            <div class="row text-center">
                <form class="form-inline">
                    <div class="form-group">
                        <input type="username" class="form-control" name="email" placeholder="Enter your username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="your password">
                    </div>
                    <button type="submit" class="btn btn-go">Login!</button>
                </form>
            </div>
            <hr>
            <div class='row text-center'>
                <h1> Sing up for free!</h1>

                <form class="form-horizontal">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" name="first_name"
                                   placeholder="First Name (E.g. Krunal )">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="last_name"
                                   placeholder="Last Name (E.g. Desai )">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email"
                                   placeholder="Email  (E.g. krunal@domain.com )">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="your password">
                        </div>
                        <div class="form-group">

                            <button type="submit" class="btn btn-go form-control">Sing up!</button>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </form>
            </div>
            <hr>
            <div class='row text-center'>
                <h1>OR</h1>

                <div class="col-md-6">
                    <a href="javascript:void(0);"><img style="height: 80px;"
                                                       src="<?php echo Yii::$app->homeUrl ?>theme-assets/images/facebook_login.png"></a>

                </div>
                <div class="col-md-6">
                    <a href="javascript:void(0);"><img style="height: 80px;"
                                                       src="<?php echo Yii::$app->homeUrl ?>theme-assets/images/facebook_login.png"></a>
                </div>

            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
    <!-- End row -->
</div><!-- End container -->
<?php echo $this->registerJs("
$(function () {
		 'use strict';
        $('#range').ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 15,
            from: 0,
            to:5,
            type: 'double',
            step: 1,
            prefix: 'Km',
            grid: true
        });
    });
"); ?>



