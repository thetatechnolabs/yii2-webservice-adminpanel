<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
    <meta name="description" content="">
    <meta name="author" content="Theta Technolabs">
    <title><?= Html::encode($this->title); ?></title>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic'
          rel='stylesheet' type='text/css'>
    <!--favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?= Yii::$app->homeUrl ?>theme-assets/icons/favicon-16x16.png">
    <link rel="manifest" href="<?= Yii::$app->homeUrl ?>theme-assets/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--favicons-->

    <?= Html::csrfMetaTags() ?>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--[if lte IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a>.</p>
<![endif]-->

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave" id="status">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div>
<!-- End Preload -->
<!-- Header ================================================== -->
<header class="<?php echo (Yii::$app->controller->id == 'home') ? '' : 'inner'; ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img class="<?php echo (Yii::$app->controller->id == 'home') ? 'hide' : ''; ?>"
                     src="/yii-backend/theme-assets/images/logo.png" height="90px" style="margin-top: -15px;">
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8" style="margin-top: 30px;">
                <div class="pull-right">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <div class="main-menu-2 <?php echo (Yii::$app->controller->id == 'home') ? '' : 'inner'; ?>">
                            <ul>
                                <li>
                                    <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('home/account') ?>"
                                       class="btn btn-go" data-target="#login_2">Login/Register</a>
                                </li>
                            </ul>
                        </div><!-- End main-menu-2 -->
                    <?php } else { ?>
                        <div class="dropdown">
                            <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="menu1"
                                    data-toggle="dropdown">
                                <img style="height: 25px; width: 25px;" class="img-circle"
                                     src="http://placehold.it/32x32"> Rohan Mashiyava
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Notification</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Favorites</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Settings</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Logout</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
        <!-- End row -->
    </div>
    <!-- End container -->
</header>
<!-- End Header =============================================== -->
<?= $content; ?>
<!-- Footer ================================================== -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-3">
                <h3>Made with <i class="fa fa-heart" style="color:red;"></i> from Ahmedabad</h3>
            </div>
            <div class="col-md-3 col-sm-3">
                <h3>About</h3>
                <ul>
                    <li><a href="about.html">About us</a></li>
                    <li><a href="faq.html">Faq</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                    <li><a href="#0" data-toggle="modal" data-target="#login_2">Login</a></li>
                    <li><a href="#0" data-toggle="modal" data-target="#register">Register</a></li>
                    <li><a href="#0">Terms and conditions</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3" id="newsletter">
                <h3>Newsletter</h3>

                <p>
                    Join our newsletter to keep be informed about offers and news.
                </p>

                <div id="message-newsletter_2">
                </div>
                <form method="post" action="assets/newsletter.php" name="newsletter_2" id="newsletter_2">
                    <div class="form-group">
                        <input name="email_newsletter_2" id="email_newsletter_2" type="email" value=""
                               placeholder="Your mail" class="form-control">
                    </div>
                    <input type="submit" value="Subscribe" class="btn_1" id="submit-newsletter_2">
                </form>
            </div>
            <div class="col-md-2 col-sm-3">
                <h3>Settings</h3>

                <div class="styled-select">
                    <select class="form-control" name="lang" id="lang">
                        <option value="English" selected>English</option>
                        <option value="French">French</option>
                        <option value="Spanish">Spanish</option>
                        <option value="Russian">Russian</option>
                    </select>
                </div>
                <div class="styled-select">
                    <select class="form-control" name="currency" id="currency">
                        <option value="USD" selected>USD</option>
                        <option value="EUR">EUR</option>
                        <option value="GBP">GBP</option>
                        <option value="RUB">RUB</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <li><a href="#0"><i class="icon-facebook"></i></a></li>
                        <li><a href="#0"><i class="icon-twitter"></i></a></li>
                        <li><a href="#0"><i class="icon-google"></i></a></li>
                        <li><a href="#0"><i class="icon-instagram"></i></a></li>
                    </ul>
                    <p>
                        © Kaatar <?php echo date('Y'); ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- End row -->
    </div>
    <!-- End container -->
</footer>
<!-- End Footer =============================================== -->

<div class="layer"></div>
<!-- Mobile menu overlay mask -->


<!-- SPECIFIC SCRIPTS -->
<?php $this->registerJs("
    $('#js-rotating').Morphext({
        animation: 'fadeIn', // Overrides default 'bounceIn'
        separator: ',', // Overrides default ','
        speed: 2300, // Overrides default 2000
        complete: function () {
            // Overrides default empty function
        }
    });
") ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
